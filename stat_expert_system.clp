;;;======================================================
;;;   Statistical Tests Expert System
;;;
;;;     This expert system gives ideas for 
;;;     statistical tests, after answering questions
;;;	    about your data 
;;;
;;;     To execute, merely load, reset and run.
;;;======================================================

;;****************
;;* DEFFUNCTIONS *
;;****************

(deffunction ask-question (?question $?allowed-values)
   (printout t ?question)
   (bind ?answer (read))
   (if (lexemep ?answer) 
       then (bind ?answer (lowcase ?answer)))
   (while (not (member ?answer ?allowed-values)) do
      (printout t ?question)
      (bind ?answer (read))
      (if (lexemep ?answer) 
          then (bind ?answer (lowcase ?answer))))
   ?answer)

(deffunction yes-or-no-p (?question)
   (bind ?response (ask-question ?question yes no y n))
   (if (or (eq ?response yes) (eq ?response y))
       then yes 
       else no))
       
(deffunction categorical-or-continuous-p (?question)
   (bind ?response (ask-question ?question categorical continuous))
   ?response
)

(deffunction exposure-groups-p (?question)
   (bind ?response (ask-question ?question one two multiple))
   ?response
)

;;;***************
;;;* QUERY RULES *
;;;***************

(defrule determine-outcome-variable ""
   (declare (salience 10))
   (not (outcome-variable ?))
   (not (test-suggestion ?))
   =>
   (assert (outcome-variable 
   (categorical-or-continuous-p "Is the outcome variable categorical or continuous? "))))
   
(defrule determine-exposure-variable ""
   (declare (salience 10))
   (not (exposure-variable ?))
   (not (test-suggestion ?))
   =>
   (assert (exposure-variable 
   (categorical-or-continuous-p "Is the exposure variable categorical or continuous? "))))
   
(defrule determine-dependency ""
   (exposure-variable ?)
   (not (dependency ?))
   (not (test-suggestion ?))
   =>
   (assert (dependency
   (yes-or-no-p "Is the data dependent (paired)? (yes/no) "))))

(defrule determine-distribution ""
   (and (outcome-variable continuous) (exposure-variable ?))
   (not (normal-distribution-data ?))
   (not (test-suggestion ?))
   =>
   (assert (normal-distribution-data
   (yes-or-no-p "Is the data normally distributed? (yes/no) "))))
   
(defrule determine-groups ""
   (dependency no)
   (exposure-variable categorical)
   (not (groups ?))
   (not (test-suggestion ?))
   =>
   (assert (groups
   (exposure-groups-p "How many groups in exposure data? (one/two/multiple) "))))
   
   
;;;****************
;;;* TEST RULES *
;;;****************

(defrule one-sample-t-test ""
   (outcome-variable continuous)
   (exposure-variable categorical)
   (dependency no)
   (normal-distribution-data yes)
   (groups one)
   =>
   (assert (test-suggestion "One sample t test (n<=30)")))

(defrule one-sample-z-test ""
   (outcome-variable continuous)
   (exposure-variable categorical)
   (dependency no)
   (normal-distribution-data yes)
   (groups one)
   =>
   (assert (test-suggestion "One sample z test (n>30)")))

(defrule two-sample-t-test ""
   (outcome-variable continuous)
   (exposure-variable categorical)
   (dependency no)
   (normal-distribution-data yes)
   (groups two)
   =>
   (assert (test-suggestion "Two sample t test")))
   
(defrule paired-t-test ""
   (outcome-variable continuous)
   (exposure-variable categorical)
   (dependency yes)
   (normal-distribution-data yes)
   =>
   (assert (test-suggestion "Paired t test")))
   
(defrule anova-test ""
   (outcome-variable continuous)
   (exposure-variable categorical)
   (dependency no)
   (normal-distribution-data yes)
   (groups multiple)
   =>
   (assert (test-suggestion "One-way ANOVA")))

(defrule pearson-test ""
   (outcome-variable continuous)
   (exposure-variable continuous)
   (dependency no)
   (normal-distribution-data yes)
   =>
   (assert (test-suggestion "Pearson Correlation")))
   
(defrule linear-regression-test ""
   (outcome-variable continuous)
   (exposure-variable continuous)
   (dependency no)
   =>
   (assert (test-suggestion "Linear Regression")))
   
   
(defrule signed-rank-test ""
   (outcome-variable continuous)
   (exposure-variable categorical)
   (dependency no)
   (normal-distribution-data no)
   (groups one)
   =>
   (assert (test-suggestion "Signed rank test")))
   
(defrule sign-test ""
   (outcome-variable continuous)
   (exposure-variable categorical)
   (dependency no)
   (normal-distribution-data no)
   (groups one)
   =>
   (assert (test-suggestion "Sign test")))

(defrule mann-whitney-u-test ""
   (outcome-variable continuous)
   (exposure-variable categorical)
   (dependency no)
   (normal-distribution-data no)
   (groups two)
   =>
   (assert (test-suggestion "Mann-Whitney U test")))
   
(defrule wilcoxon-test ""
   (outcome-variable continuous)
   (exposure-variable categorical)
   (dependency yes)
   (normal-distribution-data no)
   =>
   (assert (test-suggestion "Wilcoxon signed rank test")))
   
(defrule kruskal-wallis-test ""
   (outcome-variable continuous)
   (exposure-variable categorical)
   (dependency no)
   (normal-distribution-data no)
   (groups multiple)
   =>
   (assert (test-suggestion "Kurskall Wallis test")))

(defrule spearman-test ""
   (outcome-variable continuous)
   (exposure-variable continuous)
   (dependency no)
   (normal-distribution-data no)
   =>
   (assert (test-suggestion "Spearman Correlation")))
   
   
(defrule mcnemar-test ""
   (outcome-variable categorical)
   (exposure-variable categorical)
   (dependency yes)
   =>
   (assert (test-suggestion "McNemar's test")))

(defrule kappa-test ""
   (outcome-variable categorical)
   (exposure-variable categorical)
   (dependency yes)
   =>
   (assert (test-suggestion "Kappa statistic")))
   
(defrule chi-square-test ""
   (outcome-variable categorical)
   (exposure-variable categorical)
   (dependency no)
   =>
   (assert (test-suggestion "Chi-square test")))
   
(defrule fisher-exact-test ""
   (outcome-variable categorical)
   (exposure-variable categorical)
   (dependency no)
   (or (groups two) (groups multiple))
   =>
   (assert (test-suggestion "Fisher's exact test")))
   
(defrule logistic-regression-test ""
   (outcome-variable categorical)
   (exposure-variable continuous)
   =>
   (assert (test-suggestion "Sensitivity & specifity")))
   
(defrule roc-test ""
   (outcome-variable categorical)
   (exposure-variable continuous)
   =>
   (assert (test-suggestion "ROC")))
  

;;;********************************
;;;* STARTUP AND CONCLUSION RULES *
;;;********************************

(defrule system-banner ""
  (declare (salience 15))
  =>
  (printout t crlf crlf)
  (printout t "The Statistical Tests Expert System")
  (printout t crlf crlf))
  
(defrule print-summary ""
  (declare (salience 15))
  (exists (test-suggestion ?item))
  =>
  (printout t crlf crlf)
  (printout t "Suggested Tests:" crlf crlf))

(defrule print-tests ""
  (declare (salience 10))
  (test-suggestion ?item)
  =>
  (format t "%s%n" ?item))
