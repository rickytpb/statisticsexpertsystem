# StatisticsExpertSystem

Project in CLIPS for determining best statistical test (technique) for your data

## Usage
Load stat_expert_system.bat with CLIPS IDE. Then do  
(reset)  
(run)

Answer the questions and get your insights.

## License
GPLv3
